﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : NetworkManager
{
	public List<Player> m_players = new List<Player>();

	public GameObject m_timePanel;

	public GameObject m_networkButtonsPanel;
	public GameObject menuBackground;
	public Button m_exitButton;
	public GameObject m_mobileController;

	public GameObject m_gameResultsPanel;
	public GameObject m_waitingForSecondPlayerPanel;

	public Timer m_timer;

	private float _halfSizePlayer;
	private float _plateRadius;
	private List<Vector3> _posList;
	public GameObject Plate;

	private GameObject localPlayer;

	#region
	public static GameController Instance { get; private set; }
	#endregion

	public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId)
	{
		int playersCount = NetworkServer.connections.Count;
		if (playersCount <= 2)
		{
			m_waitingForSecondPlayerPanel.SetActive(true);
			localPlayer = Instantiate(
				playerPrefab,
				playerPrefab.transform.position,
				Quaternion.identity);

			NetworkServer.AddPlayerForConnection(conn, localPlayer, playerControllerId);

			_halfSizePlayer = localPlayer.transform.GetComponentInChildren<MeshCollider>().bounds.size.x / 2;

			_plateRadius = (Plate.transform.GetComponent<MeshCollider>().bounds.size.x / 2) - _halfSizePlayer * 2;
			localPlayer.GetComponent<Player>().Plate = Plate;

			m_players.Add(localPlayer.GetComponent<Player>());
			localPlayer.transform.position = GeneratePosition();

			if (playersCount == 2)
			{
				m_waitingForSecondPlayerPanel.SetActive(false);
				Plate.GetComponent<Plate>().isGameStarted = true;
				ReneratePostitionOfPLayers();
				m_players[0].RpcShowTime();
				m_players[1].CmdShowTime();

				m_players[1].RpcChangeMaterialOnYellow();
				m_players[1].CmdChangeMaterialOnYellow();
			}
		}
		else
		{
			conn.Disconnect();
		}
	}

	public override void OnStopHost()
	{
		Debug.Log("Host stop");
		NetworkServer.DisconnectAll();
	}

	public override void OnStopClient()
	{
		Debug.Log("Client stop");
		base.OnStopClient();
	}

	public override void OnClientDisconnect(NetworkConnection conn)
	{
		Debug.Log("OnClientDisconnect");
		base.OnClientDisconnect(conn);
		conn.Disconnect();
	}

	private void Awake()
	{
		Instance = this;
		m_timePanel.SetActive(false);
		m_gameResultsPanel.SetActive(false);
		m_exitButton.gameObject.SetActive(false);
		m_mobileController.SetActive(false);
		m_waitingForSecondPlayerPanel.SetActive(false);
	}

	void Start()
	{
		m_gameResultsPanel.SetActive(false);
	}

	// custom network hud control
	public UnityEngine.UI.Text hostNameInput;

	public void StartLocalGame()
	{
		StartHost();
		m_networkButtonsPanel.SetActive(false);
		m_gameResultsPanel.SetActive(false);
		menuBackground.SetActive(false);
		m_timePanel.SetActive(true);
		m_mobileController.SetActive(true);
		m_exitButton.gameObject.SetActive(true);
	}

	public void JoinLocalGame()
	{
		if (hostNameInput.text != "Enter your IP...")
		{
			networkAddress = hostNameInput.text;
		}
		StartClient();
		m_networkButtonsPanel.SetActive(false);
		m_gameResultsPanel.SetActive(false);
		menuBackground.SetActive(false);
		m_mobileController.SetActive(true);
		m_exitButton.gameObject.SetActive(true);
	}

	public void ExitGame()
	{
		if (NetworkServer.active)
		{
			StopServer();
		}
		if (NetworkClient.active)
		{
			StopClient();
		}
		m_players.Clear();
		m_networkButtonsPanel.SetActive(true);
		menuBackground.SetActive(true);
		m_mobileController.SetActive(false);
		m_exitButton.gameObject.SetActive(false);
		Application.Quit();
	}

	void ReneratePostitionOfPLayers()
	{
		_posList = new List<Vector3>();
		Vector3 player1Pos = GeneratePosition();
		_posList.Add(player1Pos);
		localPlayer.transform.position = player1Pos;

		Vector3 botPos = GeneratePositionExcludeListPos(_posList);
		SpawnBot(botPos);
	}

	Vector3 GeneratePosition()
	{
		return new Vector3(Random.Range(-_plateRadius, _plateRadius), playerPrefab.transform.position.y, Random.Range(-_plateRadius, _plateRadius));
	}

	Vector3 GeneratePositionExcludeListPos(List<Vector3> excludeList)
	{
		Vector3 playerPos = GeneratePosition();
		excludeList.ForEach(a =>
		{
			while (Vector3.Distance(a, playerPos) <= _halfSizePlayer)
			{
				playerPos = GeneratePosition();
			}
		});

		return playerPos;
	}
	
	public void SpawnBot(Vector3 spawnSpot)
	{
		GameObject bot = Instantiate(
			playerPrefab,
			spawnSpot,
			Quaternion.identity);
		bot.transform.position = spawnSpot;

		NetworkServer.Spawn(bot);
		bot.GetComponent<Player>().RpcChangeMaterialOnPurple();
		bot.GetComponent<Player>().CmdChangeMaterialOnPurple();
	}
	
	private void SetRndomMass(GameObject obj)
	{
		float masstoset = Random.Range(0.5f, 1);
		obj.GetComponent<Rigidbody>().mass = masstoset;
	}

	public void PlayClick()
	{
		AudioSource audioSource = GetComponent<AudioSource>();
		audioSource.Play();
	}
}
