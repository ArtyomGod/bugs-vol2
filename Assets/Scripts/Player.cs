﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;
using Random = UnityEngine.Random;

public class Player : NetworkBehaviour
{
	[SyncVar]
	private float mass = 0;
	public Material YellowLadyBug;
	public Material PurpleLadyBug;
	private NetworkAnimator m_animator;
	private bool isMassSet = false;
	float gravity = 15;

	[SyncVar]
	float m_seconds = 0;
	
	[SyncVar]
	Vector3 moveDirection = Vector3.zero;

	[SyncVar]
	bool m_moving;

	public GameObject Plate;
	private Vector3 plateCenter;
	private float plateRadius;
	private float halfSizePlayer;

	private float winTime = 0;

	void Start()
	{
		if(Plate == null) Plate = GameObject.Find("Plate");
		transform.parent = GameObject.Find("ImageTarget").transform;
		plateCenter = Plate.GetComponent<MeshRenderer>().bounds.center;
		halfSizePlayer = this.transform.GetComponentInChildren<MeshCollider>().bounds.size.x / 2;
		plateRadius = (Plate.transform.GetComponent<MeshCollider>().bounds.size.x / 2) - halfSizePlayer;
		SetRndomMass();
		transform.GetChild(1).GetComponentInChildren<TextMesh>().text = mass.ToString();
		FreezePozition();
	}

	[ClientCallback]
	void Update()
	{
		SetDistance();
		if (!isLocalPlayer)
		{
			return;
		}
		DrawLine();
		//CalculateAngle();

		if (isServer && GameController.Instance.m_players.Count == 2)
		{
			m_seconds += Time.deltaTime;
			RpcUpdateTime();
		}
		else if (!isServer)
		{
			m_seconds = GameController.Instance.m_timer.GetTime();
			CmdUpdateTime();
		}

		if (isServer && GameController.Instance.m_players.Count == 2 && !isMassSet)
		{
			RpcSetMass();
			isMassSet = true;
		}

		if (moveDirection.y > gravity * -1)
		{
			moveDirection.y -= gravity * Time.deltaTime;
		}

		if (!hasAuthority)
		{
			return;
		}

		if (Plate.GetComponent<Plate>().isWin && winTime == 0)
		{
			winTime = (float) Math.Round(GameController.Instance.m_timer.GetTime(), 2);
			RpcShowGameResultsText("Win!\n It took: " + winTime + " s");
		}
		else
		{
			UnfreezePozition();
			MovePLayer();
		}

		transform.GetChild(1).GetChild(0).transform.rotation = Quaternion.LookRotation(Camera.main.transform.forward);
		transform.GetChild(1).transform.rotation = Quaternion.LookRotation(Camera.main.transform.forward);
	}

	void FreezePozition()
	{
		this.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezePositionX;
	}

	void UnfreezePozition()
	{
		this.GetComponent<Rigidbody>().constraints &= ~RigidbodyConstraints.FreezePositionX | ~RigidbodyConstraints.FreezePositionZ;
	}
	void MovePLayer()
	{
		float x = (Input.GetAxis("Horizontal") + CrossPlatformInputManager.GetAxis("Horizontal")) * 0.5f;
		float z = (Input.GetAxis("Vertical") + CrossPlatformInputManager.GetAxis("Vertical")) * 0.5f;

		if (Math.Abs(x) < 0 && Math.Abs(z) < 0)
		{
			FreezePozition();
		}

		Vector3 movement = new Vector3(x, 0, z);
		Vector3 nextPos = this.transform.position + movement * Time.deltaTime;

		if (x != 0 && z != 0)
		{
			this.transform.eulerAngles = new Vector3(this.transform.eulerAngles.x, Mathf.Atan2(x, z) * Mathf.Rad2Deg,
				this.transform.eulerAngles.z);
		}
		
		if (Vector3.Distance(plateCenter, nextPos) < plateRadius)
		{
			this.transform.position = nextPos;
		}
	}

	[ClientCallback]
	private void LateUpdate()
	{
	}

	[ClientCallback]
	public void CmdMoveTo(Vector3 pos)
	{
		if (!isLocalPlayer) return;
		
		if (isServer)
		{
			Debug.Log("host start anim");
			if (!m_moving) CmdSetAnimationTrigger();
		}
		else
		{
			Debug.Log("client start anim");
			if (!m_moving) CmdSetAnimationTrigger();
		}
		
		m_moving = true;
	}

	public override void OnStartLocalPlayer()
	{
		base.OnStartLocalPlayer();
	}

	[Command]
	public void CmdUpdateTime()
	{
		GameController.Instance.m_timer.SetTime(m_seconds);
	}

	[ClientRpc]
	public void RpcUpdateTime()
	{
		GameController.Instance.m_timer.SetTime(m_seconds);
	}

	[ClientRpc]
	public void RpcShowTime()
	{
		GameController.Instance.m_timePanel.SetActive(true);
		GameController.Instance.m_timer.SetPaused(false);
	}

	[Command]
	public void CmdShowTime()
	{
		GameController.Instance.m_timePanel.SetActive(true);
		GameController.Instance.m_timer.SetPaused(false);
	}

	[ClientRpc]
	public void RpcShowGameResultsText(string text)
	{
		GameController.Instance.m_gameResultsPanel.GetComponentInChildren<Text>().text = text;
		GameController.Instance.m_gameResultsPanel.SetActive(true);
	}

	[Command]
	public void CmdShowGameResultsText(string text)
	{
		RpcShowGameResultsText(text);
	}

	[ClientRpc]
	public void RpcSetAnimationTrigger()
	{
		m_animator.SetTrigger("Trigger");
	}

	[Client]
	public void CmdSetAnimationTrigger()
	{
		m_animator.SetTrigger("Trigger");
	}

	[ClientRpc]
	public void RpcPlayWinningSound()
	{
		GetComponent<AudioSource>().Play();
	}

	[Command]
	public void CmdPlayWinningSound()
	{
		GetComponent<AudioSource>().Play();
	}

	public IEnumerator WaitSetAnimationTrigger() {
		yield return new WaitForSeconds(1.5f);
		Debug.Log("Stop wait animation");
		m_animator.SetTrigger("Trigger");
	}

	void OnCollisionEnter(Collision collision)
	{
		if (collision.gameObject.name == "PlayerUnit(Clone)")
		{
			Physics.IgnoreCollision(collision.collider, gameObject.GetComponentInChildren<MeshCollider>());
		}
	}

	void DrawLine()
	{
		Vector3 centerPos = Plate.GetComponent<MeshRenderer>().bounds.center + new Vector3(0, Plate.transform.localScale.y, 0);
		var lr = gameObject.GetComponent<LineRenderer>();
		lr.SetPosition(0, centerPos + new Vector3(0, 0.1f, 0));
		lr.SetPosition(1, this.transform.position + new Vector3(0, 0.3f, 0));
	}

	double ClculateDistance()
	{
		var distance = Vector3.Distance(transform.position, Plate.GetComponent<MeshRenderer>().bounds.center + new Vector3(0, Plate.transform.localScale.y, 0));
		return Math.Round(distance * 10, 0);
	}
	
	void SetDistance()
	{
		transform.GetChild(1).GetComponentInChildren<TextMesh>().text = mass.ToString() + "(" + ClculateDistance() + ")";
	}

	[Command]
	public void CmdChangeMaterialOnPurple()
	{
		this.GetComponentInChildren<MeshRenderer>().material = PurpleLadyBug;
	}

	[ClientRpc]
	public void RpcChangeMaterialOnPurple()
	{
		this.GetComponentInChildren<MeshRenderer>().material = PurpleLadyBug;
	}

	[ClientRpc]
	public void RpcChangeMaterialOnYellow()
	{
		this.GetComponentInChildren<MeshRenderer>().material = YellowLadyBug;
	}

	[Command]
	public void CmdChangeMaterialOnYellow()
	{
		this.GetComponentInChildren<MeshRenderer>().material = YellowLadyBug;
	}

	public void ChangeMaterialOnYellow()
	{
		this.GetComponentInChildren<MeshRenderer>().material = YellowLadyBug;
	}

	public void ChangeMaterialOnPurple()
	{
		this.GetComponentInChildren<MeshRenderer>().material = PurpleLadyBug;
	}

	private void SetRndomMass()
	{
		if (mass == 0)
		{
			mass = (float) Math.Round(Random.Range(0.5f, 1), 2);
		}

		RpcSetMass();
	}

	public void RpcSetMass()
	{
		this.GetComponent<Rigidbody>().mass = mass;
	}

}
